#!/bin/bash

tty -s && tput setaf 1;
echo "This script is deprecated. Please move to using the devops app:destroy command.";
tty -s && tput setaf 7;

function helptext {
echo '
Usage:  destroy-magento-instance-from-branch -- [options]

    Required:
    --branch      <argvalue>  Branch name to build an instance for
    --projectcode <argvalue>  Project code. This is used to build other args by convention if they are not passed

    Optional:
    --branchdir         <argvalue>  Working directory name for new branch (e.g."release-1.0.0")
    --branchroot        <argvalue>  Branch root (e.g. "/var/www/myproject/branches")
';
}

if [[ -z $1 ]]; then
    helptext
    exit 1
fi

while [[ $# > 0 ]]
do
key="$1"

case $key in
    help|--help)
        helptext;
        ;;

    # Required params
    --branch)
        branch="$2"
        ;;

    --projectcode)
        projectCode="$2"
        ;;

    # Optional params
    --branchdir)
        branchDir="$2"
        ;;

    --branchroot)
        branchRoot="$2"
        ;;

esac
shift # past argument
done

if [[ -z $projectCode ]]; then
    echo "--projectcode must be set" 1>&2;
    exit 1;
fi

if [[ -z $branch ]]; then
    echo "--branch must be set" 1>&2;
    exit 1;
fi

if [[ -z $branchDir ]]; then
    branchDir=$(sed "s|/|-|g" <<< $branch);
fi

if [[ -z $branchRoot ]]; then
    branchRoot="/var/www/$projectCode/branches";
fi

branchDbName=$(sed "s|[\/\.\-]|_|g" <<< $projectCode"_"$branch);

echo "
Script Variables:
Branch: $branch
Branch Database Name: $branchDbName
Branch Dir: $branchDir
Branch Root: $branchRoot
Project Code: $projectCode
";

if [[ ! -d $branchRoot ]]; then
    tty -s && tput setaf 1;
    echo "Branch root $branchRoot does not exist. Aborting.";
    tty -s && tput setaf 7;
    exit 1;
fi

cd $branchRoot;
if [[ ! -d $branchDir ]]; then
    tty -s && tput setaf 1;
    echo "Branch $branchDir does not exist. Aborting.";
    tty -s && tput setaf 7;
    exit 1;
else
    rm -rf $branchDir;
    mysql -e "DROP DATABASE IF EXISTS $branchDbName";
fi