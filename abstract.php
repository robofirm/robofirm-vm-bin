<?php

class Robofirm_Shell
{
    public function getDocumentRoot()
    {
        $path = getcwd();
        do {
            /**
             * Using a whitelist of directories here to avoid traversing into other websites. I originally had a glob here,
             * but realized it would make it all the way up to the root of all websites and then potentially find another
             * site running Magento.
             */
            if (is_file($path . '/app/Mage.php')) {
                $docRoot = $path;
                break;
            } elseif (is_file($path . '/htdocs/app/Mage.php')) {
                $docRoot = $path . '/htdocs';
                break;
            } elseif (is_file($path . '/magento/app/Mage.php')) {
                $docRoot = $path . '/magento';
                break;
            } elseif (is_file($path . '/public/app/Mage.php')) {
                $docRoot = $path . '/public';
                break;
            }

        } while ($path = preg_replace('%' . DIRECTORY_SEPARATOR . '[^' . DIRECTORY_SEPARATOR . ']+$%', '', $path));

        if (!isset($docRoot)) {
            throw new RuntimeException('Could not determine document root.');
        }

        return $docRoot;
    }
}
